@extends('layout')

@section('content')
    <div class="row">
        <div class="col-8">
            @if($post->image)
                <div
                    style="background-image: url('{{ $post->image->url() }}'); min-height: 500px; color: white; text-align: center; background-position: center; background-attachment: fixed;">
                    <h1 style="padding-top: 100px; text-shadow: 1px 2px #000">
                        @else
                            <h1>
                                @endif
                                {{ $post->title }}
                                <x-badge>
                                    @slot('show',now()->diffInMinutes($post->created_at) < 30)
                                    Brand New Post!
                                </x-badge>
                                @if($post->image)

                            </h1>
                </div>
                @else
                </h1>
            @endif
            <p>{{ $post->content }}</p>
            {{--            <img src="{{ Storage::url($post->image->path) }}" alt="">--}}
            {{--            <img src="{{ $post->image->url() }}" alt="">--}}
            <x-updated>
                @slot('date',$post->created_at )
                @slot('name',$post->user->name )
            </x-updated>

            <x-updated>
                @slot('date',$post->updated_at )
                Updated
            </x-updated>

            <x-tags>
                @slot('tags',$post->tags)
            </x-tags>
            <p> {{ trans_choice('messages.people.reading',$counter)  }}</p>
            <h4>Comments</h4>
         <x-comment-form>
             @slot('route',route('posts.comments.store',['post'=>$post->id]))
         </x-comment-form>
            <x-comment-list>
                @slot('comments',$post->comments)
            </x-comment-list>
        </div>
        <div class="col-4">
            @include('posts._activity')
        </div>
    </div>
@endsection('content')
