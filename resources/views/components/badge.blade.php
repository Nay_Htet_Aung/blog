@if(!isset($show) || $show)
    <span class="badge badge-{{ $type ?? 'success' }} m-2">
        {{ $slot }}
    </span>
@endif
