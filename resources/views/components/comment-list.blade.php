@forelse($comments as $comment)
    <p>
        {{$comment->content }},
    </p>
    <x-tags>
        @slot('tags',$comment->tags)
    </x-tags>
    <x-updated>
        @slot('date',$comment->created_at )
        @slot('name',$comment->user->name )
        @slot('userId',$comment->user->id)
    </x-updated>
@empty
    <p class="text-muted">No comments yet!</p>
@endforelse
