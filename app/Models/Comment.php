<?php

namespace App\Models;

use App\Scopes\LatestScope;
use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class Comment extends Model
{
    use HasFactory, SoftDeletes,Taggable;

    protected $fillable = ['user_id', 'content'];
//    public  function  blogPost(){
//       return  $this->belongsTo(BlogPost::class);
//    }
//    protected $hidden = [
//        'deleted_at','commentable_type','commentable_id','user_id'
//    ];
    public function commentable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeLatest(Builder $query)
    {
        return $query->orderBy(static::CREATED_AT, 'desc');
    }

//    protected static function booted()
//    {
////        static::addGlobalScope(new LatestScope());
//
//        static::creating(function (Comment $comment) {
//            if($comment->commentable_type === BlogPost::class){
//                Cache::tags(['blog-post'])->forget("blog-post-{$comment->commentable_id}");
//                Cache::tags(['blog-post'])->forget('mostCommented');
//            }
//
//        });
//    }
}
