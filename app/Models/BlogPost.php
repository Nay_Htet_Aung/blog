<?php

namespace App\Models;

use App\Scopes\DeletedAdminScope;
use App\Scopes\LatestScope;
use App\Traits\Taggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;


class BlogPost extends Model
{
    // protected $table = 'blogposts';
    use HasFactory,SoftDeletes,Taggable;

    protected $fillable = ['title', 'content','user_id'];

//    protected static function booted()
//    {
//        static::addGlobalScope(new DeletedAdminScope());
//        static::updating(function (BlogPost $blogPost){
//            Cache::tags(['blog-post'])->forget("blog-post-{$blogPost->id}");
//        });
//        static::deleting(function (BlogPost $blogPost) {
//            $blogPost->comments()->delete();
//           $blogPost->image()->delete();
//            Cache::tags(['blog-post'])->forget("blog-post-{$blogPost->id}");
//        });
//        static::restoring(function (BlogPost $blogPost) {
//            $blogPost->comments()->restore();
//        });
//    }
    public function comments()
    {
        return $this->morphMany('App\Models\Comment','commentable')->latest();
    }
    public function user(){
        return $this->belongsTo( User::class);
    }

    public function image(){
        return $this->morphOne(Image::class,'imageable');
    }
    public function scopeLatest(Builder $query)
    {
        return $query->orderBy(static::CREATED_AT, 'desc');
    }
    public function scopeMostCommented(Builder $query)
    {
        // comments_count
        return $query->withCount('comments')->orderBy('comments_count', 'desc');
    }
    public function scopeLatestWithRelations(Builder $query)
    {
        return $query->latest()
            ->withCount('comments')
            ->with('user')
            ->with('tags');
    }

}
