<?php
namespace App\Facades;

use App\Contracts\CounterContract;
use Illuminate\Support\Facades\Facade;

class CounterFacade extends Facade{
    /**
     * @method static int increment(string $key,array $tags=null)
     */
    public static function getFacadeAccessor()
    {
//        parent::getFacadeAccessor(); // TODO: Change the autogenerated stub
        return CounterContract::class;
    }
}
