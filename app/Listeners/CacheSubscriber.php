<?php

namespace App\Listeners;



use Illuminate\Cache\Events\CacheHit;
use Illuminate\Cache\Events\CacheMissed;
use Illuminate\Support\Facades\Log;

class CacheSubscriber
{
    /**
     * Handle user login events.
     */
    public function handleCacheHit($event) {
        Log::info("{$event->key} cache Hit");
    }

    /**
     * Handle user logout events.
     */
    public function handleCacheMissed($event) {
        Log::info("{$event->key} cache miss");
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            CacheHit::class,
            [CacheSubscriber::class, 'handleCacheHit']
        );

        $events->listen(
            CacheMissed::class,
            [CacheSubscriber::class, 'handleCacheMissed']
        );
    }
}
