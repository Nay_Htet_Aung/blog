<?php

namespace App\Http\Controllers;

use App\Contracts\CounterContract;
use App\Events\BlogPostPosted;
use App\Facades\CounterFacade;
use App\Models\BlogPost;
use App\Models\Image;

use Illuminate\Http\Request;
use App\Http\Requests\StorePost;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

use Illuminate\Support\Facades\Storage;


class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')
            ->only(['create', 'store', 'edit', 'update', 'destroy']);

    }

    public function index()
    {
        return view('posts.index', [
            'posts' => Auth::check() && Auth::user()->is_admin ?
                BlogPost::withTrashed()->latestWithRelations()->get()
                : BlogPost::latestWithRelations()->get(),
        ]);
    }


    public function show($id)
    {
        $blogPost = Cache::tags(['blog-post'])->remember("blog-post-{$id}", 60, function () use ($id) {
            return BlogPost::with('comments', 'tags', 'user', 'comments.user')->findOrFail($id);
        });

//        return view('posts.show', ['post' => BlogPost::with([
//            'comments' => function ($query) {
//                return $query->latest();
//            }
//        ])->findOrFail($id)]);
//       $counter=resolve(Counter::class);
//        dd($this->counter);
        return view('posts.show', [
            'post' => $blogPost,
            'counter' => CounterFacade::increment("blog-post-{$id}",['blog-post']),
        ]);
    }

    public function create()
    {
        return view('posts.create');
    }

    public function store(StorePost $request)
    {
        $validatedData = $request->validated();
        $validatedData['user_id'] = Auth::id();
        $blogPost = BlogPost::create($validatedData);
        if ($request->hasFile('thumbnail')) {
            $path = $request->file('thumbnail')->store('thumbnails');
            $blogPost->image()->save(
                Image::make(['path' => $path])
            );
        }
        event(new BlogPostPosted($blogPost));

        $request->session()->flash('status', 'Blog post was created!');

        return redirect()->route('posts.show', ['post' => $blogPost->id]);
    }

    public function edit($id)
    {
        $post = BlogPost::findOrFail($id);
//        if(Gate::denies('update-post',$post)){
//            abort(403,'You cannot edit this post');
//        }
        $this->authorize($post);
        return view('posts.edit', ['post' => $post]);
    }

    public function update(StorePost $request, $id)
    {
        $post = BlogPost::findOrFail($id);
//        if(Gate::denies('update-post',$post)){
//            abort(403,'You cannot edit this post');
//        }
        $this->authorize($post);
        $validatedData = $request->validated();

        $post->fill($validatedData);
        if ($request->hasFile('thumbnail')) {
            $path = $request->file('thumbnail')->store('thumbnails');
            if ($post->image) {
                Storage::delete($post->image->path);
                $post->image->path = $path;
                $post->image->save();
            } else {
                $post->image()->save(
                    Image::make(['path' => $path])
                );
            }
        }
        $post->save();
        $request->session()->flash('status', 'Blog post was updated!');

        return redirect()->route('posts.show', ['post' => $post->id]);
    }

    public function destroy(Request $request, $id)
    {
        $post = BlogPost::findOrFail($id);
//        if(Gate::denies('update-post',$post)){
//            abort(403,'You cannot delete this post');
//        }
        $this->authorize($post);
        $post->delete();

        // BlogPost::destroy($id);

        $request->session()->flash('status', 'Blog post was deleted!');

        return redirect()->route('posts.index');
    }
}
