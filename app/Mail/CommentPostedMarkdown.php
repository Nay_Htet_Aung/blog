<?php

namespace App\Mail;


use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class CommentPostedMarkdown extends Mailable
{
    use Queueable, SerializesModels;
    public $comment;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Comment $comment)
    {
        //
        $this->comment = $comment;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
//        return $this->from('admin@laravel.test','Admin');
//        ->view('emails.posts.commented',);

//        return $this->attach(storage_path('app/public') . '/' . $this->comment->user->image->path,[
//            'as'=>'image.jpg',
//            'mime'=>'image/jpg'
//        ])
//            ->subject($subject)->view('emails.posts.commented');
//        return $this->attachFromStorage($this->comment->user->image->path, 'profile.jpg')
//            ->subject($subject)->view('emails.posts.commented');
        $subject = "Comment was posted on your {$this->comment->commentable->title} blog post";
//        return $this->attachData(Storage::get($this->comment->user->image->path),'profile_jpg',[
//            'mime'=>'image/jpg'
//        ])->subject($subject)->view('emails.posts.commented');
        return $this->subject($subject)->markdown('emails.posts.commented-markdown');
    }
}
